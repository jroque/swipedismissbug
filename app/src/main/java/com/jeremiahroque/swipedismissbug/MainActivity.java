package com.jeremiahroque.swipedismissbug;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    View overlayView;
    View toggleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        overlayView = findViewById(R.id.overlayView);
        toggleView = findViewById(R.id.toggleView);

        toggleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (overlayView.getVisibility() != View.VISIBLE) {
                    overlayView.setAlpha(1.0f);
                    overlayView.setVisibility(View.VISIBLE);
                } else {
                    overlayView.setVisibility(View.GONE);
                }
            }
        });

        final SwipeDismissBehavior swipeDismissBehavior = new SwipeDismissBehavior();
        swipeDismissBehavior.setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_ANY);
        swipeDismissBehavior.setListener(new SwipeDismissBehavior.OnDismissListener() {
            @Override
            public void onDismiss(View view) {
                Toast.makeText(getApplicationContext(), view.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDragStateChanged(int state) {

            }
        });

        CoordinatorLayout.LayoutParams params =
                (CoordinatorLayout.LayoutParams) overlayView.getLayoutParams();
        params.setBehavior(swipeDismissBehavior);
    }
}
